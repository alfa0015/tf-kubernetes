module "kubernetes" {
  source  = "nlamirault/doks/digitalocean"
  version = "~> 0.4"

  cluster_name       = var.cluster_name
  auto_upgrade       = var.auto_upgrade
  region             = var.region
  kubernetes_version = var.kubernetes_version

  size                          = var.size
  auto_scale                    = var.auto_scale
  min_nodes                     = var.min_nodes
  max_nodes                     = var.max_nodes
  node_count                    = var.node_count
  maintenance_policy_day        = "sunday"
  maintenance_policy_start_time = "04:00"
}