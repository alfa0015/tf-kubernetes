resource "digitalocean_domain" "domain" {
  name = var.domain_name
}

# # Add a record to the domain
resource "digitalocean_record" "www_wordpress" {
  domain = digitalocean_domain.domain.name
  type   = "A"
  name   = var.website_record
  ttl    = "30"
  value  = var.LoadBalancerIP
}

# # Add a record to the domain
resource "digitalocean_record" "ftp" {
  domain = digitalocean_domain.domain.name
  type   = "A"
  name   = var.ftp_record
  ttl    = "30"
  value  = var.LoadBalancerIP
}