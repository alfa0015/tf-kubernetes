output "host" {
  description = "Kubernetes host"
  value = digitalocean_kubernetes_cluster.kubernetes-terraform.endpoint
  sensitive = true
}

output "token" {
  description = "Kubernetes token authentication"
  value = digitalocean_kubernetes_cluster.kubernetes-terraform.kube_config[0].token
  sensitive = true
}

output "cluster_ca_certificate" {
  description = "Kubernetes ca certificate"
  value = base64decode(
    digitalocean_kubernetes_cluster.kubernetes-terraform.kube_config[0].cluster_ca_certificate
  )
  sensitive = true
}