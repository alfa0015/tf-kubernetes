resource "digitalocean_kubernetes_cluster" "kubernetes-terraform" {
  name   = var.cluster_name
  region = "nyc1"
  # Grab the latest version slug from `doctl kubernetes options versions`
  version = var.k8s_version

  node_pool {
    name       = "worker-pool"
    size       = "s-2vcpu-4gb"
    node_count = 1
  }
}