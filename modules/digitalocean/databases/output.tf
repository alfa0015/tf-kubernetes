output "db_host" {
  description = "database host"
  value = digitalocean_database_cluster.mysql-website.private_host
}

output "db_user" {
  description = "database user"
  value = digitalocean_database_user.user-website.name
}

output "db_password" {
  description = "database password"
  value = digitalocean_database_user.user-website.password
  sensitive = true
}

output "db_port" {
  description = "database port"
  value = digitalocean_database_cluster.mysql-website.port
}

output "db_name" {
  description = "database name"
  value = digitalocean_database_db.database-wordpress.name
}