variable "host" {}
variable "token" {}
variable "cluster_ca_certificate" {}
variable "domain_name" {}
variable "website_record" {}
variable "ftp_record" {}
variable "ClusterIssuerName" {}