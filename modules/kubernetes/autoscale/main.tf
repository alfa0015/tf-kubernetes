resource "kubernetes_horizontal_pod_autoscaler" "website" {
  metadata {
    name = "website"
    namespace = "applications"
  }

  spec {
    max_replicas = 10
    min_replicas = 1

    scale_target_ref {
      api_version = "apps/v1"
      kind = "Deployment"
      name = var.deploymentWebsiteName
    }
    
    target_cpu_utilization_percentage = 70
  }
}