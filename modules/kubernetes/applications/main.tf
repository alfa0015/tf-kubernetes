resource "helm_release" "metrics" {
  name       = "metrics"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "metrics-server"

  namespace = "kube-system"

  set {
    name = "hostNetwork"
    value = "true"
  }

  set {
    name = "apiService.create"
    value = "true"
  }
}

resource "kubernetes_namespace" "namespace_application" {
  metadata {
    name = "applications"
    annotations = {
      name = "applications"
    }
  }
  timeouts {
    delete = "10m"
  }
}

resource "kubernetes_namespace" "namespace_ingress" {
  metadata {
    name = "nginx"
    annotations = {
      name = "nginx"
    }
  }
}

resource "helm_release" "nginx_ingress" {
  name       = "nginx-ingress-controller"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx-ingress-controller"

  namespace = "nginx"

  timeout = 1200

  set {
    name = "args[0]"
    value = "/nginx-ingress-controller"
  }

  set {
    name = "args[1]"
    value = "--default-backend-service=nginx/nginx-ingress-controller-default-backend"
  }

  set {
    name = "args[2]"
    value = "--election-id=ingress-controller-leader"
  }

  set {
    name = "args[3]"
    value = "--ingress-class=nginx"
  }

  set {
    name = "args[4]"
    value = "--configmap=nginx/nginx-ingress-controller"
  }

  set {
    name = "args[5]"
    value = "--tcp-services-configmap=applications/tcp-services"
  }

  set {
    name = "tcp.22"
    value = "nginx/sftp:22"
  }

  depends_on = [ 
    kubernetes_config_map.tcp-service,
    helm_release.sftp
  ]
}

data "kubernetes_service" "service_ingress" {
  metadata {
    name      = "nginx-ingress-controller"
    namespace = "nginx"
  }

  depends_on = [ helm_release.nginx_ingress ] 
}

resource "helm_release" "nfs_storage" {
  name       = "nfs"

  repository = "https://kvaps.github.io/charts"
  chart      = "nfs-server-provisioner"

  namespace = "kube-system"

  set {
    name  = "persistence.enabled"
    value = true
  }

  set {
    name = "persistence.storageClass"
    value = "do-block-storage"
  }

  set {
    name = "persistence.size"
    value = "50Gi"
  }
}

resource "helm_release" "website" {
  name       = "website"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "wordpress"

  namespace = "applications"

  set {
    name = "wordpressUsername"
    value = var.wordpress_username
  }

  set {
    name = "wordpressPassword"
    value = var.wordpress_password
  }

  set {
    name = "persistence.storageClass"
    value = "nfs"
  }

  set {
    name = "mariadb.primary.persistence.storageClass"
    value = "do-block-storage"
  }

  set {
    name = "service.type"
    value = "ClusterIP"
  }

  set {
    name = "memcached.enabled"
    value = true
  }

  set {
    name = "wordpressConfigureCache"
    value = true
  }

  set {
    name = "mariadb.enabled"
    value = false
  }

  set {
    name = "externalDatabase.host"
    value = var.mysql_hosts
  }

  set {
    name = "externalDatabase.port"
    value = var.mysql_port
  }

  set {
    name = "externalDatabase.user"
    value = var.mysql_user
  }

  set {
    name = "externalDatabase.password"
    value = var.mysql_password
  }

  set {
    name = "externalDatabase.database"
    value = var.mysql_database
  }

  depends_on = [
    helm_release.nfs_storage
  ]
}

resource "helm_release" "sftp" {
  name       = "sftp"

  repository = "https://emberstack.github.io/helm-charts"
  chart      = "sftp"

  namespace = "applications"
  set {
    name = "resources.requests.memory"
    value = "512Mi"
  }

  set {
    name = "resources.requests.cpu"
    value = "300m"
  }

  set {
    name = "storage.volumes[0].name"
    value = "website-wordpress"
  }

  set {
    name = "storage.volumes[0].persistentVolumeClaim.claimName"
    value = "website-wordpress"
  }

  set {
    name = "storage.volumeMounts[0].mountPath"
    value = "/home/website/wordpress"
  }

  set {
    name = "storage.volumeMounts[0].name"
    value = "website-wordpress"
  }

  set {
    name = "storage.volumeMounts[0].subPath"
    value = "wordpress"
  }
  set {
    name = "configuration.Users[0].Username"
    value = var.wordpress_username
  }
  set {
    name = "configuration.Users[0].Password"
    value = var.wordpress_password
  }
  depends_on = [
    helm_release.nfs_storage,
    helm_release.website,
  ]
}

resource "kubernetes_config_map" "tcp-service" {
  metadata {
    name = "tcp-services"
    namespace = "applications"
  }

  data = {
    "22"  = "applications/sftp:22"
  }

  depends_on = [
    helm_release.sftp
  ] 
}

resource "helm_release" "cert-manager" {
  name       = "cert-manager"

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"

  namespace = "nginx"

  set {
    name  = "installCRDs"
    value = true
  }

}

resource helm_release issuer-prod {
  depends_on = [ helm_release.cert-manager ]

  name      = "certs-production"
  namespace = "nginx"
  chart     = "${path.module}/certs"

  force_update    = true
  cleanup_on_fail = true
  recreate_pods   = false
  reset_values    = false

  set {
    name = "email"
    value = "alfa0015.rafael@gmail.com"
  }

  set {
    name = "acmeApi"
    value = "https://acme-v02.api.letsencrypt.org/directory"
  }

  set {
    name = "privateKeySecretRefName"
    value = "prod-issuer-account-key"
  }

  set {
    name = "ingressClassName"
    value = "nginx"
  }
}