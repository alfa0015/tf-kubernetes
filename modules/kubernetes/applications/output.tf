output "LoadBalancerIP" {
  value = data.kubernetes_service.service_ingress.status.0.load_balancer.0.ingress.0.ip
}

output "ClusterIssuerName" {
  value = helm_release.issuer-prod.name
}

output "deploymentWebsiteName" {
  value = "${helm_release.website.name}-wordpress"
}