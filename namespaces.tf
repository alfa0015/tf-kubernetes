resource "kubernetes_namespace_v1" "nginx-ingress" {
  depends_on = [
    module.kubernetes
  ]
  metadata {
    name = "ingress"
  }
}

resource "kubernetes_namespace_v1" "cert-manager" {
  depends_on = [
    module.kubernetes
  ]
  metadata {
    name = "cert-manager"
  }
}