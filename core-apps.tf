resource "helm_release" "metrics" {
  depends_on = [
    module.kubernetes
  ]
  name = "metrics"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "metrics-server"

  namespace = "kube-system"

  set {
    name  = "hostNetwork"
    value = "true"
  }

  set {
    name  = "apiService.create"
    value = "true"
  }
}

resource "helm_release" "nginx_ingress" {
  depends_on = [
    kubernetes_namespace_v1.nginx-ingress
  ]
  name       = "nginx-ingress-controller"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx-ingress-controller"
  namespace  = kubernetes_namespace_v1.nginx-ingress.metadata.0.name
  timeout    = 1200
  version    = "9.3.18"
}

resource "helm_release" "cert-manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  namespace  = kubernetes_namespace_v1.cert-manager.metadata.0.name
  version = "v1.10.0"
  set {
    name  = "installCRDs"
    value = true
  }

}

resource helm_release issuer-prod {
  depends_on = [ 
    helm_release.cert-manager 
  ]
  name      = "certs-production"
  namespace  = kubernetes_namespace_v1.cert-manager.metadata.0.name
  chart     = "certs"

  set {
    name = "email"
    value = "alfa0015.rafael@gmail.com"
  }

  set {
    name = "acmeApi"
    value = "https://acme-v02.api.letsencrypt.org/directory"
  }

  set {
    name = "privateKeySecretRefName"
    value = "prod-issuer-account-key"
  }

  set {
    name = "ingressClassName"
    value = "nginx"
  }
}