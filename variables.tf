variable "do_token" {
  type        = string
  description = "token to authenticate with digital ocean"
}
variable "region" {
  type        = string
  description = "region to deploy cluster"
  default     = "nyc1"
}
variable "cluster_name" {
  type        = string
  description = "name to kubernetes cluster"
  default     = "demo"
}
variable "auto_upgrade" {
  type        = bool
  description = "enable auto upgrade"
  default     = false
}
variable "kubernetes_version" {
  type        = string
  description = "kubernetes version to deploy"
}
variable "size" {
  type        = string
  description = "size droplet to nodepools"
  default     = "s-2vcpu-4gb"
}

variable "auto_scale" {
  type        = bool
  description = "enable node auto scale"
  default     = true
}

variable "min_nodes" {
  type        = number
  description = "minimum number nodes on nodepool"
  default     = 1
}

variable "max_nodes" {
  type        = number
  description = "minimum number nodes on nodepool"
  default     = 1
}

variable "node_count" {
  type        = number
  description = "The number of Droplet instances in the node pool."
  default     = 1
}

data "digitalocean_kubernetes_cluster" "cluster" {
  depends_on = [
    module.kubernetes
  ]
  name = var.cluster_name
}